<?php

use Illuminate\Support\Facades\Route;
    Route::group([
        'prefix' => 'auth'
    ], function ($router) {
        Route::post('login', 'AuthController@login');
        Route::get('logout', 'AuthController@logout')->middleware("jwt.verify");
        Route::get('me', 'AuthController@detailsProfile')->middleware("jwt.verify");
        Route::get('refresh_token', 'AuthController@refresh');
    });
    Route::group([
        'prefix' => 'articles',
        'middleware' => "jwt.verify"
    ], function ($router) {
        Route::get('', 'ArticlesController@list');
        Route::post('', 'ArticlesController@store');
        Route::put('/{id}', 'ArticlesController@update');
        Route::get('/{id}', 'ArticlesController@getById');
        Route::delete('/{id}', 'ArticlesController@delete');
    });
    Route::group([
        'prefix' => 'contacts',
        'middleware' => "jwt.verify"
    ], function ($router) {

        Route::get('', 'ContactsController@list');
        Route::post('', 'ContactsController@store');
        Route::get('/sync_contacts', 'ContactsController@syncContacts');
        Route::put('/{id}', 'ContactsController@update');
        Route::get('/{id}', 'ContactsController@getById');
        Route::delete('/{id}', 'ContactsController@deleteById');
    });
    Route::group([
        'prefix' => 'orders',
      //  'middleware' => "jwt.verify"
    ], function ($router) {
        Route::get('', 'OrdersController@list');
        Route::get('/sync_orders', 'OrdersController@syncCommands');
        Route::get('/{id}', 'OrdersController@getById');
        Route::delete('/{id}', 'OrdersController@deleteById');
        Route::get("/flow/orders_to_csv", "OrdersController@orderToCsv");
    });


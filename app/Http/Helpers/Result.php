<?php

namespace App\Http\Helpers;

use Illuminate\Database\Eloquent\Builder;

/**
 * @property array error
 */
class Result
{
    public $success;
    public $message;
    public $status;
    public $data;

    /*************************** init response API ***************************/
    public function __construct()
    {
        $this->success = false;
        $this->message = "";
        $this->status  = 200;
        $this->data    = array();
    }

    /*********************** trait success response API ********************/
    public function success($data = null, $status = 200)
    {
        $this->data    = $data;
        $this->success = true;
        $this->status  = $status;
        $this->message = trans('messages.success');
    }

    /*********************** trait exception API ***************************/
    public function fail($msg, $status = 400)
    {
        $this->success = false;
        $this->message = $msg;
        $this->status  = $status;
        $this->data    = null;
    }

    /******************** paginate result queries builder ******************/
    public function successPaginate(Builder $data, $numberObjectOfPage = 20)
    {
        $paginate      = $data->paginate($numberObjectOfPage);
        $this->data    = ['list' => $paginate->items(), 'total' => $paginate->total()];
        $this->success = true;
        $this->message = trans('messages.success');
    }
}


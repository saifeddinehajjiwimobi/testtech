<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreOrUpdateArticleBladeRequest;
use App\Http\Requests\StoreOrUpdateArticleRequest;
use App\Repositories\ArticleRepository;
use \Illuminate\Http\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;

class ArticleController extends Controller
{
    private $articleRepository;

    public function __construct(ArticleRepository $articleRepository)
    {
        $this->articleRepository = $articleRepository;
    }

    /************ delete article by id ****************/
    public function delete(string $id): RedirectResponse
    {
        $this->articleRepository->deleteArticle($id);
        /************ redirect to old screen ****************/
        return back();
    }

    /************ show form article ****************/
    public function showFormAddArticle()
    {
        return view("dashboard.articles.add_article");
    }

    public function store(Request $request)
    {
        /************* get data article form request ********************/
        $article_details = $request->request->all();
        $this->articleRepository->createArticle($article_details);
        return redirect()->route('home');
    }

    public function showFormUpdateArticle(Request $request, string $id)
    {
        //todo::create form update article
    }

    public function update(Request $request, string $id)
    {
        //todo::update info article by id
    }
}

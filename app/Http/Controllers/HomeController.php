<?php

namespace App\Http\Controllers;

use App\Repositories\ArticleRepository;
use App\Repositories\CommandRepository;
use App\Repositories\ContactRepository;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    private $commandRepository;
    private $contactRepository;
    private $articleRepository;
    public function __construct(
        CommandRepository $commandRepository,
        ContactRepository $contactRepository,
        ArticleRepository $articleRepository
    )
    {
        $this->middleware('auth');
        $this->commandRepository = $commandRepository;
        $this->contactRepository = $contactRepository;
        $this->articleRepository = $articleRepository;
    }

    public function index()
    {
        $orders = $this->commandRepository->getAllCommand([]);
        $contacts = $this->contactRepository->getAllContacts([]);
        $articles = $this->articleRepository->getAllArticles([]);
        return view('dashboard.home')
            ->with(  "orders", $orders->get())
            ->with( "contacts", $contacts->get())
            ->with("articles", $articles->get());

    }
}

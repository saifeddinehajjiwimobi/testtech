<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Helpers\Result;
use App\Http\Services\EcommerceAPI;
use App\Http\Services\ExportOrders;
use App\Repositories\CommandRepository;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

class OrdersController extends Controller
{
    private $commandRepository;

    public function __construct()
    {
        $this->commandRepository = new CommandRepository();
    }

    public function list(): JsonResponse
    {
        $res = new Result();
        try {
            $list = $this->commandRepository->getAllCommand([]);
            $res->successPaginate($list);
        } catch (\Exception $exception) {
            $res->fail($exception->getMessage());
        }
        return response()->json($res, $res->status);
    }

    public function syncCommands(): JsonResponse
    {
        $res = new Result();
        try {
            $res = (new EcommerceAPI())->SyncCommandsApi(true);
        } catch (\Exception $exception) {
            $res->fail($exception->getMessage());
        }
        return response()->json($res, $res->status);
    }

    public function getById(string $command_id): JsonResponse
    {
        $res = new Result();
        try {
            $list = $this->commandRepository->getCommandById($command_id);
            $res->success($list);
        } catch (\Exception $exception) {
            $res->fail($exception->getMessage());
        }
        return response()->json($res, $res->status);
    }

    /******************* delete order and his details ******************/
    public function deleteById(string $command_id): JsonResponse
    {
        $res = new Result();
        try {
            $command = $this->commandRepository->deleteCommand($command_id);
            $res->success(["deleted_status" => $command]);
        } catch (\Exception $exception) {
            $res->fail($exception->getMessage());
        }
        return response()->json($res, $res->status);
    }

    /******************* export order ******************/
    public function orderToCsv(Request $request)
    {

        $res = new Result();
        try {
            if ($request->query->has("link_file") && $request->query->get("link_file") == "true") {
                $res = (new ExportOrders())->getCsvLink($request, $res);
            } else {
                (new ExportOrders())->getCsvContent();
            }
        } catch (\Exception $exception) {
            $res->fail($exception->getMessage());
        }
        return response()->json($res, $res->status);
    }
}

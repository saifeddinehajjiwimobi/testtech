<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Helpers\Result;
use App\Http\Requests\StoreOrUpdateRequestContact;
use App\Http\Services\EcommerceAPI;
use App\Models\Contact;
use App\Repositories\ContactRepository;
use Brick\Math\Exception\DivisionByZeroException;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;

class ContactsController extends Controller
{
/**
 * @var ContactRepository
 */
private $contactRepository;
    public function __construct()
    {
        $this->contactRepository = new ContactRepository();
    }
    /***************** get pagination list of contacts ***************/
    public function list(Request $request): JsonResponse
    {
        $res = new Result();
        try {
            $list = $this->contactRepository->getAllContacts();
            $res->successPaginate($list);
        } catch (\Exception $exception) {
            $res->fail($exception->getMessage());
        }
        return response()->json($res, $res->status);
    }

    /***************** get contact by id ***************/
    public function getById(string $id): JsonResponse
    {
        $res = new Result();
        try {
            $contact = $this->contactRepository->getContactById($id);
            $res->success($contact);
        } catch (\Exception $exception) {
            $res->fail($exception->getMessage());
        }
        return response()->json($res, $res->status);
    }

    /***************** delete contact by id ***************/
    public function deleteById(string $id): JsonResponse
    {
        $res = new Result();
        try {
            $contact = $this->contactRepository->deleteContact($id);
            $res->success(["deleted_status" => $contact]);
        } catch (\Exception $exception) {
            $res->fail($exception->getMessage());
        }
        return response()->json($res, $res->status);
    }

    /***************** validation && update  contact by id ***************/
    public function update(string $id, StoreOrUpdateRequestContact $request): JsonResponse
    {
        $res = new Result();
        try {
            $contact_details = $request->getAttributes();
            $contact_existing = $this->contactRepository->getContactById($id);
            if (!$contact_existing) {
                throw new \Exception("L'identifiant de contact ne exsite pas.");
            }
            $contact = $this->contactRepository->updateContact($id, $contact_details);
            $res->success($contact);
        } catch (\Exception $exception) {
            $res->fail($exception->getMessage());
        }
        return response()->json($res, $res->status);
    }

    /***************** add new contact ***************/
    public function store(StoreOrUpdateRequestContact $request): JsonResponse
    {
        $res = new Result();
        try {
            $contact_details = $request->getAttributes();
            $contact_existing = Contact::find($contact_details['id']);
            if ($contact_existing) {
                throw new \Exception("L'identifiant de contact est déjà exsite.");
            }
            $contact = $this->contactRepository->createContact($contact_details);
            $res->success($contact);
        }
        catch (\Exception $exception) {
            $res->fail($exception->getMessage());
        }
        return response()->json($res, $res->status);
    }

    /***************** sync list contacts in database && API REST ***************/
    public function syncContacts(): JsonResponse
    {
        $res = new Result();
        try {
            $res = (new EcommerceAPI())->SyncContactsApi(true);
        } catch (\Exception $exception) {
            $res->fail($exception->getMessage());
        }
        return response()->json($res, $res->status);
    }
}

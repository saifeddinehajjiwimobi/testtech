<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\BaseAuthController;
use App\Http\Helpers\Result;
use App\Models\User;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class AuthController extends BaseAuthController
{
    public function __construct()
    {
        parent::__construct();
        /***************** check permissions functions ***************/
        $this->middleware('auth:api', ['except' => ['login']]);
    }
    public function login(Request $request): JsonResponse
    {
        $res = new Result();
        try {
            $credentials = $request->all(['email','password']);
            if (! $token = auth('api')->attempt($credentials)) {
                return response()->json(['error' => 'Unauthorized'], 401);
            }
            $user = User::where('email', $credentials['email'])->get();

            $response = $this->respondWithToken($token);
            $response['user'] = $user;
            $res->success($response);
        } catch (\Exception $exception) {
            $res->fail($exception->getMessage());
        }
        return response()->json($res, $res->status);
    }

    /***************** destroy token  jwt ***************/
    public function logout(): JsonResponse
    {
        $res = new Result();
        try {
            auth("api")->logout();
            $res->success();
        } catch (\Exception $exception) {
            $res->fail($exception->getMessage());
        }
        return response()->json($res, $res->status);
    }
    /**
     * Refresh a token.
     *
     * @return JsonResponse
     */

    public function refresh(): JsonResponse
    {
        $res=new Result();
        try {
            $data = $this->respondWithToken(auth("api")->refresh());
            $res->success($data);
        }
        catch (\Exception $e)
        {
            $res->fail("token is invalid");
        }
        return response()->json($res,$res->status);
    }

    /***************** get current user  by token jwt ***************/
    public function detailsProfile(): JsonResponse
    {
        $res = new Result();
        try {
          $res = $this->user();
        } catch (\Exception $exception) {
            $res->fail($exception->getMessage());
        }
        return response()->json($res, $res->status);
    }
    /**
     * Get the token array structure.
     *
     * @param string $token
     *
     * @return array
     */
    /***************** custom response login or refresh functions ***************/
    protected function respondWithToken(string $token): array
    {
        return [
            'token_type'=>"Bearer",
            'token' => $token,
            'expires_in' =>auth('api')->factory()->getTTL() * 60
        ];
    }


}

<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Helpers\Result;
use App\Http\Requests\StoreOrUpdateArticleRequest;
use App\Models\Article;
use App\Repositories\ArticleRepository;
use Illuminate\Http\JsonResponse;

class ArticlesController extends Controller
{
    private $articleRepository;

    public function __construct()
    {
        $this->articleRepository = new ArticleRepository();
    }

    /********************* get list of articles ********************/
    public function list():JsonResponse
    {
        $res = new Result();
        try {
            $list = $this->articleRepository->getAllArticles([]);
            /****************** custom pagination result *****************/
            $res->successPaginate($list);
        } catch (\Exception $exception) {
            $res->fail($exception->getMessage());
        }
        return response()->json($res, $res->status);
    }
    /********************* delete article by ID ****************/
    public function delete(string $article_id) : JsonResponse{
        $res = new Result();
        try {
            $article = $this->articleRepository->deleteArticle($article_id);
            $res->success(["deleted_status" => $article]);
        } catch (\Exception $exception) {
            $res->fail($exception->getMessage());
        }
        return response()->json($res, $res->status);
    }
    /********************* get article by id **************/
    public function getById(string $article_id) : JsonResponse{
        $res = new Result();
        try {
            $article = $this->articleRepository->getArticleById($article_id);
            $res->success($article);
        } catch (\Exception $exception) {
            $res->fail($exception->getMessage());
        }
        return response()->json($res, $res->status);
    }
    /********************* add new article  **************/
    public function store(StoreOrUpdateArticleRequest $request): JsonResponse
    {
        $res = new Result();
        try {
            $article_details = $request->getAttributes();
            $article_existing = Article::find($article_details['id']);
            if ($article_existing) {
                throw new \Exception("L'identifiant de article est déjà exsite.");
            }
            $article = $this->articleRepository->createArticle($article_details);
            $res->success($article);
        }
        catch (\Exception $exception) {
            $res->fail($exception->getMessage());
        }
        return response()->json($res, $res->status);
    }
    /********************* update infos article by id  **************/
    public function update(string $id, StoreOrUpdateArticleRequest $request): JsonResponse
    {
        $res = new Result();
        try {
            $article_details = $request->getAttributes();
            $article_existing = $this->articleRepository->getArticleById($id);
            if (!$article_existing) {
                throw new \Exception("L'identifiant de article ne exsite pas.");
            }
            $contact = $this->articleRepository->updateArticle($id, $article_details);
            $res->success($contact);
        } catch (\Exception $exception) {
            $res->fail($exception->getMessage());
        }
        return response()->json($res, $res->status);
    }
}

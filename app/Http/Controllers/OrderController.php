<?php

namespace App\Http\Controllers;

use App\Http\Services\ExportOrders;
use Illuminate\Http\Request;

class OrderController extends Controller
{
    public function exportOrders() {
        (new ExportOrders())->getCsvContent();
    }
}

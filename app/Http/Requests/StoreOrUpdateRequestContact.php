<?php

namespace App\Http\Requests;

use App\Http\Helpers\Result;
use Illuminate\Foundation\Http\FormRequest;
use  \Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Http\JsonResponse;

class StoreOrUpdateRequestContact extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'id' => 'required|string|max:255',
            'account_name' => 'required|string|max:255',
            'address_line_one' => 'nullable|string|max:255',
            'address_line_two' => 'nullable|string|max:255',
            'city' => 'nullable|string|max:100',
            'country' => 'nullable|string|max:100',
            'contact_name' => 'nullable|string|max:100',
            'zip_code' => 'nullable|string|max:10'
        ];
    }
    public function getAttributes(): array
    {
        return array_merge(
            $this->only(["id", "account_name", "address_line_one", "address_line_two", "city", "contact_name", "country", "zip_code"])
        );
    }

    /**
     * Handle a failed validation attempt.
     *
     * @return mixed
     */

    public function failedValidation(Validator $validator)
    {
        $res = new Result();
        $res->fail($validator->errors()->first());
        throw new HttpResponseException(response()->json($res, $res->status));
    }
}

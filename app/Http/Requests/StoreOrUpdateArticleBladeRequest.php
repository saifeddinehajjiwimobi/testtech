<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreOrUpdateArticleBladeRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "id" => 'required|string|max:255',
            "description" => 'nullable|string',
            "item_description" => 'nullable|string',
            "unit_description" => 'nullable|string',
            "unit_price" => 'required|numeric',
            "unit_code" => 'nullable|string',
        ];
    }
    public function  getAttributes(): array
    {
        return array_merge(
            $this->only(
                [
                    "id",
                    "description",
                    "item_description",
                    "unit_description",
                    "unit_price",
                    "unit_code"
                ]
            )
        );
    }
}

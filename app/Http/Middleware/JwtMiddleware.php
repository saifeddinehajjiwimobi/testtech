<?php

namespace App\Http\Middleware;

use App\Http\Helpers\Result;
use Closure;
use Illuminate\Http\Request;
use Tymon\JWTAuth\Facades\JWTAuth;

class JwtMiddleware
{

    /**
     * Handle an incoming request.
     *
     * @param Request $request
     * @param Closure $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        $res = new Result();
        try {
            $user = JWTAuth::parseToken()->authenticate();
            if (!isset($user['id'])) {
                throw new \Exception("failed");
            }
        } catch (\Tymon\JWTAuth\Exceptions\TokenInvalidException $e) {
            $res->fail('Token is Invalid');
            return response()->json($res, 400);
        } catch (\Tymon\JWTAuth\Exceptions\TokenExpiredException $e) {
            $res->fail('Token is Expired');
            return response()->json($res, 403);
        } catch (\Exception $e) {
            $res->fail('Authorization Token not found');
            return response()->json($res, 401);
        }
        return $next($request);
    }
}

<?php

namespace App\Http\Services;

use App\Http\Helpers\Result;
use App\Models\Command;
use Illuminate\Http\Request;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Csv;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

class ExportOrders
{
    public function getCsvContent()
    {
        $spreadsheet = $this->preparedSpreadsheet();
        $filename = 'export_' . date('d-m-y') . '.xls';
        $writer = new Xlsx($spreadsheet);
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment; filename="' . urlencode($filename) . '"');
        $writer->save('php://output');
    }

    public function preparedSpreadsheet(): Spreadsheet
    {
        $spreadsheet = new Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();
        /*************************** prepared header Spreadsheet ****************/
        $sheet->setTitle('Orders List');
        $sheet->setCellValue('A1', 'order');
        $sheet->setCellValue('B1', 'delivery_name');
        $sheet->setCellValue('C1', 'delivery_address');
        $sheet->setCellValue('D1', 'delivery_country');
        $sheet->setCellValue('E1', 'delivery_zipcode');
        $sheet->setCellValue('F1', 'delivery_city');
        $sheet->setCellValue('G1', 'items_count');
        $sheet->setCellValue('H1', 'item_index');
        $sheet->setCellValue('I1', 'item_id');
        $sheet->setCellValue('J1', 'item_quantity');
        $sheet->setCellValue('K1', 'line_price_excl_vat');
        $sheet->setCellValue('L1', 'line_price_incl_vat');

        /*************************** resize auto columns Spreadsheet ****************/
        foreach (range('A', 'L') as $letter) {
            $sheet->getColumnDimension($letter)->setAutoSize(true);
        }
        /******************** get commands ********************/
        $commands = Command::all();
        $indexRow = 2;
        foreach ($commands as $command) {

            /****************** read commands in Spreadsheet *************/
            $command_lines = $command->salesOrderLines;
            $total_amount_vat = 0;
            $total_amount_no_vat = 0;
            $total_quantity = 0;
            foreach ($command_lines as $key => $command_line) {
                $sheet->setCellValue('A' . $indexRow, "---");
                $sheet->setCellValue('B' . $indexRow, "---");
                $sheet->setCellValue('C' . $indexRow, "---");
                $sheet->setCellValue('D' . $indexRow, "---");
                $sheet->setCellValue('E' . $indexRow, "---");
                $sheet->setCellValue('F' . $indexRow, "---");
                $sheet->setCellValue('G' . $indexRow, "---");
                $sheet->setCellValue('H' . $indexRow, $key + 1);
                $sheet->setCellValue('I' . $indexRow, $command_line->article_id);
                $sheet->setCellValue('J' . $indexRow, $command_line->quantity);
                $sheet->setCellValue('K' . $indexRow, $command_line->amount);
                $sheet->setCellValue('L' . $indexRow, $command_line->amount + $command_line->vat_amount);
                $total_amount_vat = $total_amount_vat + $command_line->amount + $command_line->vat_amount;
                $total_amount_no_vat = $total_amount_no_vat + $command_line->amount;
                $total_quantity = $total_quantity + $command_line->quantity;
                $indexRow = $indexRow + 1;
            }
            $indexRow = $indexRow + 1;
            $sheet->setCellValue('A' . $indexRow, $command->id);
            $sheet->setCellValue('B' . $indexRow, isset($command->contact) ? $command->contact->contact_name : null);
            $sheet->setCellValue('C' . $indexRow, isset($command->contact) ? $command->contact->address_line_one : null);
            $sheet->setCellValue('D' . $indexRow, isset($command->contact) ? $command->contact->country : null);
            $sheet->setCellValue('E' . $indexRow, isset($command->contact) ? $command->contact->zip_code : null);
            $sheet->setCellValue('F' . $indexRow, isset($command->contact) ? $command->contact->city : null);
            $sheet->setCellValue('G' . $indexRow, count($command->salesOrderLines));
            $sheet->setCellValue('H' . $indexRow, "");
            $sheet->setCellValue('I' . $indexRow, "");
            $sheet->setCellValue('J' . $indexRow, $total_quantity);
            $sheet->setCellValue('K' . $indexRow, $total_amount_no_vat);
            $sheet->setCellValue('L' . $indexRow, $total_amount_vat);
            $indexRow = $indexRow + 3;
        }


        /***************** return Spreadsheet ******************/
        return $spreadsheet;
    }

    public function getCsvLink(Request $request, Result &$res): Result
    {
        try {
            $spreadsheet = $this->preparedSpreadsheet();
            $filename = 'export_orders.xlsx';
            $writer = new Xlsx($spreadsheet);
            //  $writer->setUseBOM(true);
            $writer->save($filename);
            $path = $request->getSchemeAndHttpHost() . '/' . $filename;
            $res->success(['path' => $path]);
        } catch (\Exception $exception) {
            $res->fail($exception->getMessage());
        }
        return $res;
    }
}

<?php

namespace App\Http\Services;

use App\Http\Helpers\Result;
use App\Jobs\SyncCommands;
use App\Jobs\SyncContacts;
use GuzzleHttp\Client;
use GuzzleHttp\Psr7\Request;

class EcommerceAPI
{
    private static $base_url = "https://4ebb0152-1174-42f0-ba9b-4d6a69cf93be.mock.pstmn.io";
    private $api_key = "";
    private $client;
    public function __construct()
    {
        $this->api_key = env("X_API_KEY");
        $this->client =  new Client();

    }

    function SyncContactsApi(bool $sync_contacts_database = true): Result
    {
        $res = new Result();
        try {
            /*********************** call api /contacts  and get list contacts from api ****************/
            $request = new Request('GET',
                self::$base_url . '/contacts',
                [ 'x-api-key' => $this->api_key ]
            );
            $result = $this->client->sendAsync($request)->wait();
            $result = json_decode($result->getBody());

            /*********************** call background task ****************/
            if ($sync_contacts_database) {
                dispatch(new SyncContacts($result->results));
            }
            $res->success($result->results);
        } catch (\Exception $exception) {
            $res->fail($exception->getMessage());
        }
        return $res;
    }
    function SyncCommandsApi(bool $sync_commands_database = true): Result
    {
        $res = new Result();
        try {

            /*********************** call api /orders and get list commands from api  ****************/
            $request = new Request('GET',
                self::$base_url . '/orders',
                [ 'x-api-key' => $this->api_key ]
            );
            $result = $this->client->sendAsync($request)->wait();
            $result = json_decode($result->getBody());

            /*********************** call background task ****************/
            if ($sync_commands_database) {
                dispatch(new SyncCommands($result->results));
            }
            $res->success($result->results);
        } catch (\Exception $exception) {
            $res->fail($exception->getMessage());
        }
        return $res;
    }
}

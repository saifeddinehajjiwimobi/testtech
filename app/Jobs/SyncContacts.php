<?php

namespace App\Jobs;

use App\Repositories\ContactRepository;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class SyncContacts implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    private $contacts;
    private $contactRepository;
    public function __construct(array $contacts)
    {
        $this->contacts = $contacts;
        $this->contactRepository = new ContactRepository();
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        foreach ($this->contacts as $contact) {

            /************** prepared Data contact ***************/
            $contact = [
                "id" => $contact->ID,
                "account_name" => $contact->AccountName,
                "address_line_one" => $contact->AddressLine1,
                "address_line_two" => $contact->AddressLine2,
                "city" => $contact->City,
                "contact_name" => $contact->ContactName,
                "country" => $contact->Country,
                "zip_code" => $contact->ZipCode
            ];
            $this->contactRepository->createOrUpdateContact($contact);
        }

    }
}

<?php

namespace App\Jobs;

use App\Repositories\CommandRepository;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\DB;

class SyncCommands implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    private $commands ;
    private $commandRepository;
    public function __construct($commands)
    {
        $this->commands = $commands;
        $this->commandRepository = new CommandRepository();
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {

        try {
            //dd($this->commands);
            /********************* sync commands API with local DataBase ***********************/
            foreach ($this->commands as $key => $command) {

                /******** prepared object command *************/
                    $data_command = [
                        "id" => $command->OrderID,
                        "amount" => $command->Amount,
                        "currency" => $command->Currency,
                        "deliver_to_id" => $command->DeliverTo,
                        "order_number" => $command->OrderNumber,
                        "command_lines" => isset($command->SalesOrderLines)? $command->SalesOrderLines->results : null
                    ];
              $this->commandRepository->createOrUpdateCommand($data_command);

            }
         } catch (\Exception $exception) {
        }


    }
}

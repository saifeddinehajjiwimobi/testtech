<?php

namespace App\Interfaces;

interface ContactRepositoryInterface
{
    public function getAllContacts(array $filters);
    public function getContactById(string  $contact_id);
    public function deleteContact(string  $contact_id);
    public function createContact(array $contact_details);
    public function updateContact(string $contact_id, array $contact_details);
    public function createOrUpdateContact(array $contact_details);
}

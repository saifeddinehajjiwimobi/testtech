<?php

namespace App\Interfaces;

interface CommandLineRepositoryInterface
{
    public function getAllCommandLinesByCommandId(string $command_id);
    public function getCommandLineById(string  $command_line_id);
    public function deleteCommandLine(string  $command_line_id);
    public function createCommandLine(array $new_command_line);
    public function updateCommandLine(string  $command_line_id, array $new_command_line);
    public function createOrUpdateCommandLine(array $command_line);
}

<?php

namespace App\Interfaces;

interface ArticleRepositoryInterface
{
    public function getAllArticles(array $filters);
    public function getArticleById(string  $article_id);
    public function deleteArticle(string  $article_id);
    public function createArticle(array $article_details);
    public function updateArticle(string  $article_id, array $new_article_details);
    public function createOrUpdateArticle(array $article_details);
}

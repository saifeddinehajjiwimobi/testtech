<?php

namespace App\Interfaces;

interface CommandRepositoryInterface
{
    public function getAllCommand(array $filters);
    public function getCommandById(string  $command_id);
    public function deleteCommand(string  $command_id);
    public function createCommand(array $command);
    public function updateCommand(string  $command_id, array $new_command_details);
    public function createOrUpdateCommand(array $command_details);
}

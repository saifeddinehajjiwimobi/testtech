<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Contact extends Model
{
    public $incrementing = false;
    use HasFactory;
    protected $casts = [
        "created_at" => "timestamp",
        "updated_at" => "timestamp"
    ];

    protected $table = "contacts";
    protected $fillable = ["id", "account_name", "address_line_one", "address_line_two", "city", "contact_name", "country", "zip_code"];
}

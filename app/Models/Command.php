<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use \Illuminate\Database\Eloquent\Relations\HasMany;
use \Illuminate\Database\Eloquent\Relations\BelongsTo;

class Command extends Model
{
    public $incrementing = false;
    protected $table = "commands";
    protected $fillable = ["id", "amount", "currency", "deliver_to_id", "order_number"];

    protected $with = ["salesOrderLines", "contact"];
    protected $withCount = ["salesOrderLines"];

    public function salesOrderLines(): HasMany
    {
        return $this->hasMany(CommandLine::class, "order_id");

    }

    /**
     * Get the user that owns the phone.
     */
    public function contact(): BelongsTo
    {
        return $this->belongsTo(Contact::class, "deliver_to_id");
    }
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Article extends Model
{
    public $incrementing = false;
    protected $fillable = [
        "id",
        "description",
        "item_description",
        "unit_description",
        "unit_price",
        "unit_code"
    ];
    use HasFactory;
}

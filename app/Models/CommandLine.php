<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CommandLine extends Model
{
    use HasFactory;
    protected $table = "command_lines";
    protected $fillable = [
        "amount",
        "discount",
        "quantity",
        "vat_amount",
        "vat_percentage",
        "article_id",
        "order_id"
    ];

}

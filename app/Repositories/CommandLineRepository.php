<?php

namespace App\Repositories;

use App\Interfaces\CommandLineRepositoryInterface;

class CommandLineRepository implements CommandLineRepositoryInterface
{

    public function getAllCommandLinesByCommandId(string $command_id)
    {
        // TODO: Implement getAllCommandLinesByCommandId() method.
    }

    public function getCommandLineById(string $command_line_id)
    {
        // TODO: Implement getCommandLineById() method.
    }

    public function deleteCommandLine(string $command_line_id)
    {
        // TODO: Implement deleteCommandLine() method.
    }

    public function createCommandLine(array $new_command_line)
    {
        // TODO: Implement createCommandLine() method.
    }

    public function updateCommandLine(string $command_line_id, array $new_command_line)
    {
        // TODO: Implement updateCommandLine() method.
    }

    public function createOrUpdateCommandLine(array $command_line)
    {
        // TODO: Implement createOrUpdateCommandLine() method.
    }
}

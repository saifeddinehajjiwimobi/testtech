<?php

namespace App\Repositories;
use App\Interfaces\ArticleRepositoryInterface;
use App\Models\Article;
use Illuminate\Database\Eloquent\Builder;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class ArticleRepository implements ArticleRepositoryInterface
{


    public function getAllArticles(array $filters = []): Builder
    {
        return Article::query()->oldest();
    }

    public function getArticleById(string $article_id)
    {
        if(!$article_id || !Article::find($article_id)) {
            throw new \Exception("article introuvable");
        }
        return Article::find($article_id);
    }

    public function deleteArticle(string $article_id): int
    {
        $article = Article::find($article_id);
        if (!$article) {
            throw new NotFoundHttpException("article introuvable");
        }
        return $article->delete();
    }

    public function createArticle(array $article_details)
    {
        return Article::create($article_details);
    }

    public function updateArticle(string $article_id, array $new_article_details)
    {
        return tap(Article::where('id', $article_id))
            ->update($new_article_details)
            ->first();
    }

    public function createOrUpdateArticle(array $article_details)
    {
        // TODO: Implement createOrUpdateArticle() method.
    }
}

<?php

namespace App\Repositories;

use App\Interfaces\CommandRepositoryInterface;
use App\Models\Article;
use App\Models\Command;
use App\Models\CommandLine;
use App\Models\Contact;
use Exception;
use Illuminate\Database\Eloquent\Builder;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class CommandRepository implements CommandRepositoryInterface
{

    public function getAllCommand(array $filters): Builder
    {
        return Command::query()->oldest();
    }

    /**
     * @throws Exception
     */
    public function getCommandById(?string $command_id)
    {
        if(!$command_id || !Command::find($command_id)) {
            throw new Exception("command introuvable");
        }
        return Command::find($command_id);
    }

    public function deleteCommand(string $command_id): int
    {
        $command = Command::find($command_id);
        if (!$command) {
            throw new NotFoundHttpException("command introuvable");
        }
        return $command->delete();
    }

    public function createCommand(array $command)
    {
        // TODO: Implement createCommand() method.
    }

    public function updateCommand(string $command_id, array $new_command_details)
    {
        // TODO: Implement updateCommand() method.
    }

    public function createOrUpdateCommand(array $command_details) : void
    {
        /***************** check excite Contact *********/
        Contact::updateOrCreate([ "id" => $command_details['deliver_to_id'] ], ["id" => $command_details['deliver_to_id'] ]);
        /***************** get command_lines from command_details *********/
        $command_lines = $command_details['command_lines'] ?? null;
        /***************** unset command_lines from command_details *********/
        unset($command_details['command_lines']);
        $command = Command::updateOrCreate([ "id" => $command_details['id']], $command_details);
        if ($command) {
            $command_id = $command->id;
         //  CommandLine::where('order_id', $command_id)->destory();
            foreach ($command_lines as $key => $command_line) {
                $article_details = [
                    "id" => $command_line->Item,
                    "description" => $command_line->Description,
                    "item_description" => $command_line->ItemDescription,
                    "unit_description"  => $command_line->UnitCode,
                    "unit_price" => $command_line->UnitPrice,
                    "unit_code" => $command_line->UnitCode
                ];

                $article = Article::updateOrCreate([ "id" => $article_details['id']], $article_details);
                if ($article) {
                    $article_id = $article->id;
                    $command_line_details  = [
                        "amount" => $command_line->Amount,
                        "discount" => $command_line->Discount,
                        "quantity" => $command_line->Quantity,
                        "vat_amount" => $command_line->VATAmount,
                        "vat_percentage" => $command_line->VATPercentage,
                     "article_id" => $article_id,
                       "order_id" => $command_id
                        ];
                  CommandLine::create($command_line_details);
                }
            }
        }

    }
}

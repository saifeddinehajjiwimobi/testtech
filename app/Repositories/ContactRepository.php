<?php

namespace App\Repositories;

use App\Interfaces\ContactRepositoryInterface;
use App\Models\Contact;
use Illuminate\Database\Eloquent\Builder;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class ContactRepository implements ContactRepositoryInterface
{

    public function getAllContacts(array $filters = []): Builder
    {
        return Contact::query()->oldest();
    }

    public function getContactById(? string $contact_id) : ?Contact
    {

        if(!$contact_id || !Contact::find($contact_id)) {
            throw new \Exception("contact introuvable");
        }
        return Contact::find($contact_id);
    }

    public function deleteContact(string $contact_id): int
    {
        $contact = Contact::find($contact_id);
        if (!$contact) {
            throw new NotFoundHttpException("contact introuvable");
        }
      return $contact->delete();
    }

    public function createContact(array $contact_details)
    {
       return Contact::create($contact_details);
    }

    public function updateContact(string $contact_id, array $contact_details)
    {
       return tap(Contact::where('id', $contact_id))
            ->update($contact_details)
            ->first();
    }

    public function createOrUpdateContact(array $contact_details) : void
    {
        Contact::updateOrCreate(["id" => $contact_details['id']], $contact_details);
    }
}

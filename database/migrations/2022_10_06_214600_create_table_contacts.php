<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTableContacts extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('contacts', function (Blueprint $table) {
            $table->engine = 'InnoDB'; // <- add this
            $table->string("id")->primary();
            $table->string("account_name")->nullable();
            $table->string("address_line_one")->nullable();
            $table->string("address_line_two")->nullable();
            $table->string("city")->nullable();
            $table->string("contact_name")->nullable();
            $table->string("country")->nullable();
            $table->string("zip_code")->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('contacts');
    }
}

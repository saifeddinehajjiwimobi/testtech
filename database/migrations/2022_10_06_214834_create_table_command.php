<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTableCommand extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('commands', function (Blueprint $table) {
            $table->engine = 'InnoDB'; // <- add this
            $table->string("id")->primary();
            $table->string("currency")->default("EUR");
            $table->string("order_number")->nullable();
            $table->double("amount")->nullable();
            $table->string('deliver_to_id')->unique();
            $table->foreign("deliver_to_id")
                ->references('id')
                ->on('contacts')
                ->onDelete('cascade')
                ->onUpdate('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('commands');
    }
}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTableCommandLines extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('command_lines', function (Blueprint $table) {
            $table->engine = 'InnoDB'; // <- add this
            $table->id();
            $table->double("amount")->nullable();
            $table->double("discount")->nullable();
            $table->integer("quantity")->nullable();
            $table->double("vat_amount")->nullable();
            $table->double("vat_percentage")->nullable();
            $table->string('article_id');
           $table->foreign("article_id")->references('id')->on('articles')->onDelete('cascade');
            $table->string('order_id');
            $table->foreign("order_id")->references('id')->on('commands')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('Command_lignes');
    }
}

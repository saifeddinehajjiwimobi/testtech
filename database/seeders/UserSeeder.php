<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
                'email'      => 'quantic_factory@gmail.com',
                'password'   => bcrypt("quanticfactory"),
                'full_name' => 'quantic factory'
            ]
        );

    }
}

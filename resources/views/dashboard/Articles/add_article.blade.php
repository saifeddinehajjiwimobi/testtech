@extends('layouts.home')
@section('content')
    <div class="row mb-4" style="overflow-x: hidden">
        <h1> Ajout Article : </h1>
        <br>
        <form action="{{route("store_article")}}" method="post">
            @csrf
            <div class="row">
                <div class="col-md-6">
                    <div class="input-group input-group-outline my-3">
                        <label class="form-label">Identifiant</label>
                        <input type="text" name="id" class="form-control">
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="input-group input-group-outline my-3">
                        <label class="form-label">Nom</label>
                        <input type="text" name="item_description" class="form-control">
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="input-group input-group-outline my-3">
                        <label class="form-label">Unite</label>
                        <input type="text" name="unit_description" class="form-control">
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="input-group input-group-outline  my-3">
                        <label class="form-label">Prix</label>
                        <input type="number" name="unit_price" class="form-control" required>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="input-group input-group-outline  my-3">
                        <label class="form-label">Code</label>
                        <input type="text" name="unit_code" class="form-control">
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="input-group input-group-outline my-3">
                        <label class="form-label">Description</label>
                        <textarea type="text" name="description" class="form-control" rows="10">
                            </textarea>
                    </div>
                </div>
            </div>
            <br>
            <div style="width: 200px; float: right">
                <button class="btn bg-gradient-info w-100 mb-0 toast-btn" type="submit" data-target="infoToast">Confirmer</button>
            </div>
        </form>
    </div>
@endsection

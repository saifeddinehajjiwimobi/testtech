@extends('layouts.home')
@section('content')
    <style>
        .table td, .table th {
            white-space: nowrap;
            padding-left: 27px;
        }
    </style>
<div class="row">
    <div class="col-xl-3 col-sm-6 mb-xl-0 mb-4">
        <div class="card">
            <div class="card-header p-3 pt-2">
                <div
                    class="icon icon-lg icon-shape bg-gradient-dark shadow-dark text-center border-radius-xl mt-n4 position-absolute">
                    <i class="material-icons opacity-10">weekend</i>
                </div>
                <div class="text-end pt-1">
                    <p class="text-sm mb-0 text-capitalize">Nombre des commandes</p>
                    <h4 class="mb-0">{{DB::table("commands")->count()}}</h4>
                </div>
            </div>
            <hr class="dark horizontal my-0">
        </div>
    </div>
    <div class="col-xl-3 col-sm-6 mb-xl-0 mb-4">
        <div class="card">
            <div class="card-header p-3 pt-2">
                <div
                    class="icon icon-lg icon-shape bg-gradient-primary shadow-primary text-center border-radius-xl mt-n4 position-absolute">
                    <i class="material-icons opacity-10">person</i>
                </div>
                <div class="text-end pt-1">
                    <p class="text-sm mb-0 text-capitalize">Nombre des clients</p>
                    <h4 class="mb-0">{{DB::table("contacts")->count()}}</h4>
                </div>
            </div>
            <hr class="dark horizontal my-0">
        </div>
    </div>
    <div class="col-xl-3 col-sm-6 mb-xl-0 mb-4">
        <div class="card">
            <div class="card-header p-3 pt-2">
                <div
                    class="icon icon-lg icon-shape bg-gradient-success shadow-success text-center border-radius-xl mt-n4 position-absolute">
                    <i class="material-icons opacity-10">store</i>
                </div>
                <div class="text-end pt-1">
                    <p class="text-sm mb-0 text-capitalize">Nombre des articles</p>
                    <h4 class="mb-0">{{DB::table("articles")->count()}}</h4>
                </div>
            </div>
            <hr class="dark horizontal my-0">
        </div>
    </div>
    <div class="col-xl-3 col-sm-6">
        <div class="card">
            <div class="card-header p-3 pt-2">
                <div
                    class="icon icon-lg icon-shape bg-gradient-info shadow-info text-center border-radius-xl mt-n4 position-absolute">
                    <i class="material-icons opacity-10">account_balance_wallet</i>
                </div>
                <div class="text-end pt-1">
                    <p class="text-sm mb-0 text-capitalize">Totale de vente</p>
                    <h4 class="mb-0">{{DB::table("commands")->sum("amount")}} EUR</h4>
                </div>
            </div>
            <hr class="dark horizontal my-0">

        </div>
    </div>
</div>
<br><br>
<div class="row mb-4" style="overflow-x: hidden">
    <div class="col-lg-12 col-md-6 mb-md-0 mb-4">
        <div class="card">
            <div class="card-header pb-0">
                <div class="row">
                    <div class="col-lg-6 col-7">
                        <h6>Commands</h6>
                        <p class="text-sm mb-0">
                        </p>
                    </div>
                    <div class="col-lg-6 col-5 my-auto text-end">
                        <div class="dropdown float-lg-end pe-4">
                            <a class="cursor-pointer" id="dropdownTable" data-bs-toggle="dropdown"
                               aria-expanded="false">
                                <i class="fa fa-ellipsis-v text-secondary"></i>
                            </a>
                            <ul class="dropdown-menu px-2 py-3 ms-sm-n4 ms-n5" aria-labelledby="dropdownTable">
                                <li><a class="dropdown-item border-radius-md" href="{{route("orders_to_csv")}}">Exporter</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card-body px-0 pb-2">
                <div class="table-responsive">
                    <table class="table align-items" style="overflow-x: hidden">
                        <thead>
                        <tr>
                            <th class=" text-secondary font-weight-bolder opacity-7">
                                Order Id
                            </th>
                            <th class="text-secondary font-weight-bolder opacity-7">
                                Order Number
                            </th>
                            <th class=" text-secondary  font-weight-bolder opacity-7">
                                Nom De client
                            </th>
                            <th class=" text-secondary  font-weight-bolder opacity-7 ps-2">
                                Prix
                            </th>
                            <th>...</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($orders as $order)
                            <tr>
                                <td>
                                    {{$order->id}}
                                </td>
                                <td >
                                    {{$order->order_number}}
                                </td>
                                <td >
                                    {{$order->contact->contact_name}}
                                </td>
                                <td >
                                    <span class="text-xs font-weight-bold">  {{$order->amount}} {{$order->currency}}</span>
                                 </td>
                                <td>
                                    <button class="btn btn-outline-primary btn-sm mb-0">View All</button>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row mb-4" style="overflow-x: hidden">
    <div class="col-lg-12 col-md-6 mb-md-0 mb-4">
        <div class="card">
            <div class="card-header pb-0">
                <div class="row">
                    <div class="col-lg-6 col-7">
                        <h6>Contacts</h6>
                        <p class="text-sm mb-0">
                        </p>
                    </div>
                </div>
            </div>
            <div class="card-body px-0 pb-2">
                <div class="table-responsive">
                    <table class="table align-items" style="overflow-x: hidden">
                        <thead>
                        <tr>
                            <th class=" text-secondary font-weight-bolder opacity-7">
                                Contact Id
                            </th>
                            <th class="text-secondary font-weight-bolder opacity-7">
                                Contact Name
                            </th>
                            <th class=" text-secondary  font-weight-bolder opacity-7">
                                Contact Address
                            </th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($contacts as $contact)
                            <tr>
                                <td>
                                    {{$contact->id}}
                                </td>
                                <td >
                                    {{$contact->account_name}}
                                </td>
                                <td >
                                    {{$contact->city}}, {{$contact->zip_code}}, {{$contact->country}}
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row mb-4" style="overflow-x: hidden">
    <div class="col-lg-12 col-md-6 mb-md-0 mb-4">
        <div class="card">
            <div class="card-header pb-0">
                <div class="row">
                    <div class="col-lg-6 col-7">
                        <h6>Articles</h6>
                        <p class="text-sm mb-0">
                        </p>
                    </div>
                    <div class="col-lg-6 col-5 my-auto text-end">
                        <div class="dropdown float-lg-end pe-4">
                            <a class="cursor-pointer" id="dropdownTable" data-bs-toggle="dropdown"
                               aria-expanded="false">
                                <i class="fa fa-ellipsis-v text-secondary"></i>
                            </a>
                            <ul class="dropdown-menu px-2 py-3 ms-sm-n4 ms-n5" aria-labelledby="dropdownTable">
                                <li><a class="dropdown-item border-radius-md" href="{{route("add_article_form")}}">Ajouter</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card-body px-0 pb-2">
                <div class="table-responsive">
                    <table class="table align-items" style="overflow-x: hidden">
                        <thead>
                        <tr>
                            <th class=" text-secondary font-weight-bolder opacity-7">
                                 Id
                            </th>
                            <th class="text-secondary font-weight-bolder opacity-7">
                                 Nom
                            </th>
                            <th class=" text-secondary  font-weight-bolder opacity-7">
                                Unite
                            </th>
                            <th class=" text-secondary  font-weight-bolder opacity-7">
                                Prix
                            </th>
                            <th class=" text-secondary  font-weight-bolder opacity-7">
                                Unite code
                            </th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($articles as $article)
                            <tr>
                                <td>
                                    {{$article->id}}
                                </td>
                                <td>
                                    {{$article->item_description}}
                                </td>
                                <td >
                                    {{$article->unit_description}}
                                </td>
                                <td >
                                    {{$article->unit_price}}
                                </td>
                                <td >
                                    {{$article->unit_code}}
                                </td>
                                <td>
                                   <button>
                                       <div class="ms-auto text-end">
                                           <a class="btn btn-link text-danger text-gradient px-3 mb-0" href="{{route("delete_article", $article->id)}}"><i class="material-icons text-sm me-2">delete</i>Delete</a>
                                           <a class="btn btn-link text-dark px-3 mb-0" href="javascript:;"><i class="material-icons text-sm me-2">edit</i>Edit</a>
                                       </div>
                                   </button>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

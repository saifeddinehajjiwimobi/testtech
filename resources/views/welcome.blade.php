@extends('layouts.home')
@section('content')
    <div class="row">
        <div class="col-xl-3 col-sm-6 mb-xl-0 mb-4">
            <div class="card">
                <div class="card-header p-3 pt-2">
                    <div
                        class="icon icon-lg icon-shape bg-gradient-dark shadow-dark text-center border-radius-xl mt-n4 position-absolute">
                        <i class="material-icons opacity-10">weekend</i>
                    </div>
                    <div class="text-end pt-1">
                        <p class="text-sm mb-0 text-capitalize">Nombre des commandes</p>
                        <h4 class="mb-0">{{DB::table("commands")->count()}}</h4>
                    </div>
                </div>
                <hr class="dark horizontal my-0">
            </div>
        </div>
        <div class="col-xl-3 col-sm-6 mb-xl-0 mb-4">
            <div class="card">
                <div class="card-header p-3 pt-2">
                    <div
                        class="icon icon-lg icon-shape bg-gradient-primary shadow-primary text-center border-radius-xl mt-n4 position-absolute">
                        <i class="material-icons opacity-10">person</i>
                    </div>
                    <div class="text-end pt-1">
                        <p class="text-sm mb-0 text-capitalize">Nombre des clients</p>
                        <h4 class="mb-0">{{DB::table("contacts")->count()}}</h4>
                    </div>
                </div>
                <hr class="dark horizontal my-0">
            </div>
        </div>
        <div class="col-xl-3 col-sm-6 mb-xl-0 mb-4">
            <div class="card">
                <div class="card-header p-3 pt-2">
                    <div
                        class="icon icon-lg icon-shape bg-gradient-success shadow-success text-center border-radius-xl mt-n4 position-absolute">
                        <i class="material-icons opacity-10">person</i>
                    </div>
                    <div class="text-end pt-1">
                        <p class="text-sm mb-0 text-capitalize">Nombre des articles</p>
                        <h4 class="mb-0">{{DB::table("articles")->count()}}</h4>
                    </div>
                </div>
                <hr class="dark horizontal my-0">
            </div>
        </div>
        <div class="col-xl-3 col-sm-6">
            <div class="card">
                <div class="card-header p-3 pt-2">
                    <div
                        class="icon icon-lg icon-shape bg-gradient-info shadow-info text-center border-radius-xl mt-n4 position-absolute">
                        <i class="material-icons opacity-10">weekend</i>
                    </div>
                    <div class="text-end pt-1">
                        <p class="text-sm mb-0 text-capitalize">Totale de vente</p>
                        <h4 class="mb-0">{{DB::table("commands")->sum("amount")}} EUR</h4>
                    </div>
                </div>
                <hr class="dark horizontal my-0">

            </div>
        </div>
    </div>
    <br><br>
    <div class="row mb-4" style="overflow-x: hidden">
        <div class="col-lg-12 col-md-6 mb-md-0 mb-4">
            <div class="card">
                <div class="card-header pb-0">
                    <div class="row">
                        <div class="col-lg-6 col-7">
                            <h6>Commands</h6>
                            <p class="text-sm mb-0">
                            </p>
                        </div>
                        <div class="col-lg-6 col-5 my-auto text-end">
                            <div class="dropdown float-lg-end pe-4">
                                <a class="cursor-pointer" id="dropdownTable" data-bs-toggle="dropdown"
                                   aria-expanded="false">
                                    <i class="fa fa-ellipsis-v text-secondary"></i>
                                </a>
                                <ul class="dropdown-menu px-2 py-3 ms-sm-n4 ms-n5" aria-labelledby="dropdownTable">
                                    <li><a class="dropdown-item border-radius-md" href="{{route("orders_to_csv")}}">Exporter</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card-body px-0 pb-2">
                    <div class="table-responsive">
                        <table class="table align-items" style="overflow-x: hidden">
                            <thead>
                            <tr>
                                <th class=" text-secondary font-weight-bolder opacity-7">
                                    Order Id
                                </th>
                                <th class="text-secondary font-weight-bolder opacity-7">
                                    Order Number
                                </th>
                                <th class=" text-secondary  font-weight-bolder opacity-7">
                                    Nom De client
                                </th>
                                <th class=" text-secondary  font-weight-bolder opacity-7 ps-2">
                                    Prix
                                </th>
                                <th>...</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($orders as $order)
                                <tr>
                                    <td>
                                        {{$order->id}}
                                    </td>
                                    <td>
                                        {{$order->order_number}}
                                    </td>
                                    <td>
                                        {{$order->contact->contact_name}}
                                    </td>
                                    <td class="align-middle text-center text-sm">
                                            <span
                                                class="text-xs font-weight-bold">  {{$order->amount}} {{$order->currency}}</span>
                                    </td>
                                    <td>
                                        <button> details</button>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
